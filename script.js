console.log('*** JS Objects ***');

/*Javascript Objects
A data type that is used to represent real world objects

- information are stored in key:value pair
- Key = properties

In JS, most core JS Features like Strings and Arrays are Objects
- Arrays are collection of data
- Strings are collection of characters

-Different
*/

let gradesArray = [98, 94, 89, 90];

let grades = {
	firstGrading: 98,
	secondingGrading: 94,
	thirdGrading: 89,
	fourtGrading: 90
};
console.log(grades);

let user = {
	firstName: 'John',
	lastName: 'Doe',
	age: 25,
	location: {
		city: 'Tokyo',
		country: 'Japan'
	},
	emails: [
		'john@mail.com',
		'johndoe@mail.com'
	],
	fullName: function(){
		return this.firstName + " " + this.lastName;
	}
};
console.log(user);
console.log(user.fullName());

//Creating Objects
/* 1. using object initializers/literal notation
This creates/declares an object and also initializes/assigns its properties upon creation

A cellphone is an example of a real world object. It has its own properties such as name, color, weight, unit model, etc.
Syntax:
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}

*/

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999
};

console.log('Result from using initializers');
console.log(cellphone);
console.log(typeof cellphone);


/*2. using constructor function
Creates a reusable funcion to create several objects that have the same data structure. This useful for creating multiple instances/copies of an object
*class is first letter capital letter
-better if maraming object na gagawin
Syntax:
	function ObjectName(keyA, keyB){
		this.keyA = keyA;
		this.keyB = keyB;
	}

	let newObject = new ObjectName(keyA, keyB)
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

let laptop = new Laptop('Lenovo', 2008);
console.log('Result from using object constructors:');
console.log(laptop);

let myLaptop = new Laptop('Macbook Air', 2020);
console.log('Result from using object constructors:');
console.log(myLaptop);

let oldLaptop = Laptop('Portal R2E CCMC', 1980); //in every instance of an object laging may "new" kung hindi magiging undefined
console.log("Result without the 'new' keyword");
console.log(oldLaptop);


//Creating empty object
let computer = {};
let myComputer = new Object();

//Accessing Object Properties
/* Two ways of accessing objects
1. using the dot notation
*/
console.log('Result from dot notation: ' + myLaptop.name);

/*2. using the square bracket notation*/
console.log('Result from square bracket: ' + myLaptop['name']);

let array = [laptop, myLaptop];

console.log(array[0]['name']);
console.log(array[0].name); //best practice


//Initializing Object Properties

let car = {}

car.name = "Honda Civic";
console.log('Result from adding dot notation');
console.log(car);


car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']); //undefined
console.log(car.manufactureDate); //undefined
console.log('Result from using square bracket notation');
console.log(car);

//Deleting Object properties
delete car['manufacture date'];
console.log('Result from deleting properties:');
console.log(car);

//Reassigning properties
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties:');
console.log(car);

//Object Methods
/*A method is a function which is a property of an object
Similar to functions/features of a real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name
			);
	}
};

console.log(person);
console.log('Result from object method');
person.talk();

person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.')
}

person.walk();

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas',
	},
	emails: [
		'joe@mail.com',
		'joesmit@mail.com'
		],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
	},

	receiver: function(){
		console.log('This is my address ' + this.address.city + ', ' + this.address.country);
	}
};

friend.introduce();
console.log(friend.address.city);
console.log(friend.emails[0]);

friend.receiver();

/* Real World Application of Objects
-Scenario
1. We would like to create a game that would have several pokemon interact with each other
2. Every pokemon would have the same set of stats, properties and functions
*/

//Using object literals to create multiple kinds of pokemon would be time consuming

let myPokemon = {
	name: "Pikachu",
	level: 3,
	healthy: 100,
	attack: 50,
	tackle: function(){
		console.log('This Pokemon tackled targetPokemon');
	},
	faint: function(){
		console.log("Pokemon fainted.")
	}
}

console.log(myPokemon);

//Creating an object consutor instead will help with this process

function Pokemon(name, level){

	//Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to _targetPokemonHealth_");
	},
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);
let bulbasaur = new Pokemon("Bulbasaur", 83);

pikachu.tackle(rattata);

pikachu.faint(rattata);

pikachu.tackle(bulbasaur);